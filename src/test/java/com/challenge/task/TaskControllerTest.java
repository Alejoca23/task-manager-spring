package com.challenge.task;

import com.challenge.task.model.TaskDTO;
import com.challenge.task.model.TaskStatus;
import com.challenge.task.request.TaskCreateRequest;
import com.challenge.task.request.TaskUpdateRequest;
import com.challenge.user.model.UserDTO;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.hateoas.EntityModel;

import java.time.LocalDateTime;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

class TaskControllerTest {

    @Mock
    private TaskService taskService;
    @Mock
    private TaskModelAssembler taskModelAssembler;
    @InjectMocks
    private TaskController taskController;

    private AutoCloseable autoCloseable;
    private TaskDTO task;
    private UserDTO userDTO;
    private EntityModel<TaskDTO> taskEntityModel;

    @BeforeEach
    void setUp() {
        autoCloseable = MockitoAnnotations.openMocks(this);
        taskController = new TaskController(taskService, taskModelAssembler);
        //Init
        userDTO = new UserDTO(1, "James",
                "james@test.com", LocalDateTime.now());
        task = new TaskDTO(1, "Call grandpa",
                "It was some days ago when we talk each other and I want to know how is him",
                LocalDateTime.now(), TaskStatus.IN_PROGRESS, userDTO);
        //Init entityModel
        taskEntityModel = EntityModel.of(task,
                linkTo(methodOn(TaskController.class).getTaskById(task.id())).withSelfRel(),
                linkTo(methodOn(TaskController.class).getTasks(task.userDTO().id())).withRel("tasks"));
    }

    @AfterEach
    void tearDown() throws Exception {
        autoCloseable.close();
    }

    @Test
    void canGetTasks() {
        //when
        when(taskService.getAllTasks(any(Integer.class))).thenReturn(List.of(task));
        //then
        assertNotNull(taskController.getTasks(task.userDTO().id()));
    }

    @Test
    void canGetTaskById() {
        //when
        when(taskService.getTask(any(Integer.class))).thenReturn(task);
        when(taskModelAssembler.toModel(any(TaskDTO.class))).thenReturn(taskEntityModel);
        //then
        assertNotNull(taskController.getTaskById(task.id()));
    }

    @Test
    void canCreateTask() {
        //given
        TaskCreateRequest request = new TaskCreateRequest(task.userDTO().id(), task.title(), task.description());
        //when
        when(taskService.createTask(any(TaskCreateRequest.class))).thenReturn(task);
        when(taskModelAssembler.toModel(any(TaskDTO.class))).thenReturn(taskEntityModel);
        //then
        assertNotNull(taskController.createTask(request));

    }

    @Test
    void canUpdateTask() {
        //given
        TaskUpdateRequest request = new TaskUpdateRequest(task.userDTO().id(), task.title(), task.description());
        //when
        when(taskService.updateTask(any(Integer.class), any(TaskUpdateRequest.class))).thenReturn(task);
        when(taskModelAssembler.toModel(any(TaskDTO.class))).thenReturn(taskEntityModel);
        //then
        assertNotNull(taskController.updateTask(1, request));
    }

    @Test
    void deleteTask() {
        //when
        doNothing().when(taskService).deleteTask(any(Integer.class));
        taskController.deleteTask(any(Integer.class));
        //then
        verify(taskService).deleteTask(any(Integer.class));
    }

    @Test
    void taskInProgressStatusWhenStatusIsInProgress() {
        //when
        when(taskService.getTask(any(Integer.class))).thenReturn(task);
        when(taskService.taskInProgress(any(Integer.class))).thenReturn(task);
        when(taskModelAssembler.toModel(any(TaskDTO.class))).thenReturn(taskEntityModel);
        //then
        assertNotNull(taskController.taskInProgressStatus(1));
    }

    @Test
    void taskInProgressStatusWhenStatusIsTodo() {
        //given
        TaskDTO newTask = new TaskDTO(1, "Call grandpa",
                "It was some days ago when we talk each other and I want to know how is him",
                LocalDateTime.now(), TaskStatus.TODO, userDTO);
        //when
        when(taskService.getTask(any(Integer.class))).thenReturn(newTask);
        when(taskService.taskInProgress(any(Integer.class))).thenReturn(newTask);
        when(taskModelAssembler.toModel(any(TaskDTO.class))).thenReturn(taskEntityModel);
        //then
        assertNotNull(taskController.taskInProgressStatus(1));
    }

    @Test
    void taskDoneStatusWhenStatusIsNotDone() {
        //when
        when(taskService.getTask(any(Integer.class))).thenReturn(task);
        when(taskService.taskDone(any(Integer.class))).thenReturn(task);
        when(taskModelAssembler.toModel(any(TaskDTO.class))).thenReturn(taskEntityModel);
        //then
        assertNotNull(taskController.taskDoneStatus(1));
    }

    @Test
    void taskDoneStatusWhenStatusIsDone() {
        //given
        TaskDTO newTask = new TaskDTO(1, "Call grandpa",
                "It was some days ago when we talk each other and I want to know how is him",
                LocalDateTime.now(), TaskStatus.DONE, userDTO);
        //when
        when(taskService.getTask(any(Integer.class))).thenReturn(newTask);
        when(taskService.taskDone(any(Integer.class))).thenReturn(newTask);
        when(taskModelAssembler.toModel(any(TaskDTO.class))).thenReturn(taskEntityModel);
        //then
        assertNotNull(taskController.taskDoneStatus(1));
    }
}