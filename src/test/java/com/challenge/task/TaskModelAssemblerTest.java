package com.challenge.task;

import com.challenge.task.model.TaskDTO;
import com.challenge.task.model.TaskStatus;
import com.challenge.user.model.UserDTO;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.hateoas.EntityModel;

import java.time.LocalDateTime;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.instanceOf;
import static org.junit.jupiter.api.Assertions.assertNotNull;

class TaskModelAssemblerTest {
    private UserDTO userDTO;
    private final TaskModelAssembler taskModelAssembler = new TaskModelAssembler();


    @BeforeEach
    void setUp() {
        //Init
        userDTO = new UserDTO(1, "James",
                "james@test.com", LocalDateTime.now());
    }

    @Test
    void toModel() {
        //given
        TaskDTO newTask = new TaskDTO(1, "Call grandpa",
                "It was some days ago when we talk each other and I want to know how is him",
                LocalDateTime.now(), TaskStatus.IN_PROGRESS, userDTO);
        //when
        EntityModel<TaskDTO> result = taskModelAssembler.toModel(newTask);

        //then
        assertThat(result, instanceOf(EntityModel.class));
        assertNotNull(result);
    }
}