package com.challenge.task;

import com.challenge.task.exception.TaskNotFoundException;
import com.challenge.task.model.Task;
import com.challenge.task.model.TaskDTOMapper;
import com.challenge.task.model.TaskStatus;
import com.challenge.task.request.TaskCreateRequest;
import com.challenge.task.request.TaskUpdateRequest;
import com.challenge.user.UserRepository;
import com.challenge.user.model.User;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.*;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

class TaskServiceTest {

    @Mock
    private UserRepository userRepository;
    @Mock
    private TaskRepository taskRepository;
    @Spy
    private TaskDTOMapper taskDTOMapper;

    @InjectMocks
    private TaskService taskService;

    private AutoCloseable autoCloseable;

    private Task task;
    private User user;

    @BeforeEach
    void setUp() {
        autoCloseable = MockitoAnnotations.openMocks(this);
        taskService = new TaskService(userRepository, taskRepository, taskDTOMapper);
        //Init user
        user = new User();
        user.setUserId(1);
        user.setUserName("George");
        user.setUserEmail("george@test.com");
        user.setCreatedDate(LocalDateTime.now());
        user.setLastUpdatedDate(LocalDateTime.now());
        //Init a task
        task = new Task();
        task.setTaskId(1);
        task.setTaskTitle("Call grandpa");
        task.setTaskDescription("It was some days ago when we talk each other and I want to know how is him");
        task.setCreatedDate(LocalDateTime.now());
        task.setLastUpdatedDate(LocalDateTime.now());
        task.setStatus(TaskStatus.TODO);
        task.setUser(user);

    }

    @AfterEach
    void tearDown() throws Exception {
        autoCloseable.close();
    }

    @Test
    void canGetAllTasks() {
        //when
        when(taskRepository.findAll()).thenReturn(List.of(task));
        taskService.getAllTasks(user.getUserId());
        //then
        verify(taskRepository).findAll();
    }

    @Test
    void canGetTaskById() {
        //when
        when(taskRepository.findById(any(Integer.class))).thenReturn(Optional.of(task));
        taskService.getTask(any(Integer.class));
        //then
        verify(taskRepository).findById(any(Integer.class));
    }

    @Test
    void getTaskByIdWillThrowWhenTaskWasNotFound() {
        //when
        when(taskRepository.findById(any(Integer.class))).thenReturn(Optional.empty());
        //then
        assertThrows(TaskNotFoundException.class, () -> taskService.getTask(any(Integer.class)));
    }


    @Test
    void canCreateTask() {
        //given
        TaskCreateRequest request = new TaskCreateRequest(user.getUserId(), task.getTaskTitle(), task.getTaskDescription());
        //when
        when(userRepository.findById(any(Integer.class))).thenReturn(Optional.of(user));
        when(taskRepository.save(any(Task.class))).thenReturn(task);
        taskService.createTask(request);
        //then
        ArgumentCaptor<Task> taskArgumentCaptor = ArgumentCaptor.forClass(Task.class);
        verify(taskRepository).save(taskArgumentCaptor.capture());
        Task capturedTask = taskArgumentCaptor.getValue();
        assertNotNull(capturedTask);
    }

    @Test
    void canUpdateWhenTaskIsFound() {
        //given
        TaskUpdateRequest request = new TaskUpdateRequest(user.getUserId(), task.getTaskTitle(), task.getTaskDescription());
        //when
        when(userRepository.findById(any(Integer.class))).thenReturn(Optional.of(user));
        when(taskRepository.findById(any(Integer.class))).thenReturn(Optional.of(task));
        when(taskRepository.save(any(Task.class))).thenReturn(task);
        taskService.updateTask(task.getTaskId(), request);
        //then
        ArgumentCaptor<Task> taskArgumentCaptor = ArgumentCaptor.forClass(Task.class);
        verify(userRepository).findById(any(Integer.class));
        verify(taskRepository).findById(any(Integer.class));
        verify(taskRepository).save(taskArgumentCaptor.capture());
        Task capturedTask = taskArgumentCaptor.getValue();
        assertNotNull(capturedTask);
    }

    @Test
    void canUpdateWhenTaskIsNotFound() {
        //given
        TaskUpdateRequest request = new TaskUpdateRequest(user.getUserId(), task.getTaskTitle(), task.getTaskDescription());
        //when
        when(userRepository.findById(any(Integer.class))).thenReturn(Optional.of(user));
        when(taskRepository.save(any(Task.class))).thenReturn(task);
        taskService.updateTask(task.getTaskId(), request);
        //then
        ArgumentCaptor<Task> taskArgumentCaptor = ArgumentCaptor.forClass(Task.class);
        verify(userRepository).findById(any(Integer.class));
        verify(taskRepository).save(taskArgumentCaptor.capture());
        Task capturedTask = taskArgumentCaptor.getValue();
        assertNotNull(capturedTask);
    }

    @Test
    void canDeleteTask() {
        //when
        doNothing().when(taskRepository).deleteById(any(Integer.class));
        taskService.deleteTask(any(Integer.class));
        //then
        verify(taskRepository).deleteById(any(Integer.class));
    }

    @Test
    void canChangeStatusTaskInProgress() {
        //when
        when(taskRepository.findById(any(Integer.class))).thenReturn(Optional.of(task));
        when(taskRepository.save(any(Task.class))).thenReturn(task);
        taskService.taskInProgress(any(Integer.class));
        //then
        ArgumentCaptor<Task> taskArgumentCaptor = ArgumentCaptor.forClass(Task.class);
        verify(taskRepository).findById(any(Integer.class));
        verify(taskRepository).save(taskArgumentCaptor.capture());
        Task capturedTask = taskArgumentCaptor.getValue();
        assertNotNull(capturedTask);
    }

    @Test
    void canChangeStatusTaskDone() {
        //when
        when(taskRepository.findById(any(Integer.class))).thenReturn(Optional.of(task));
        when(taskRepository.save(any(Task.class))).thenReturn(task);
        taskService.taskDone(any(Integer.class));
        //then
        ArgumentCaptor<Task> taskArgumentCaptor = ArgumentCaptor.forClass(Task.class);
        verify(taskRepository).findById(any(Integer.class));
        verify(taskRepository).save(taskArgumentCaptor.capture());
        Task capturedTask = taskArgumentCaptor.getValue();
        assertNotNull(capturedTask);
    }
}