package com.challenge.user;

import com.challenge.user.model.UserDTO;
import com.challenge.user.request.UserCreateRequest;
import com.challenge.user.request.UserUpdateRequest;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.hateoas.EntityModel;

import java.time.LocalDateTime;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

class UserControllerTest {

    @Mock
    private UserService userService;
    @Mock
    private UserModelAssembler userModelAssembler;

    @InjectMocks
    private UserController userController;

    private AutoCloseable autoCloseable;
    private UserDTO userDTO;
    private EntityModel<UserDTO> entityModel;

    @BeforeEach
    void setUp() {
        autoCloseable = MockitoAnnotations.openMocks(this);
        userController = new UserController(userService, userModelAssembler);
        //Init userDTO
        userDTO = new UserDTO(1, "James",
                "james@test.com", LocalDateTime.now());
        //Init entityModel
        entityModel = EntityModel.of(userDTO,
                linkTo(methodOn(UserController.class).getUserById(userDTO.id())).withSelfRel(),
                linkTo(methodOn(UserController.class).getUsers()).withRel("users"));
    }

    @AfterEach
    void tearDown() throws Exception {
        autoCloseable.close();
    }

    @Test
    void canGetUsers() {
        when(userService.getAllUsers()).thenReturn(List.of(userDTO));
        assertNotNull(userController.getUsers());
    }

    @Test
    void canGetUserById() {
        when(userService.getUserById(any(Integer.class))).thenReturn(userDTO);
        when(userModelAssembler.toModel(any(UserDTO.class))).thenReturn(entityModel);
        assertNotNull(userController.getUserById(1));
    }

    @Test
    void canAddUser() {
        //given
        UserCreateRequest userCreateRequest = new UserCreateRequest(userDTO.name(), userDTO.email());
        //when
        when(userService.createUser(any(UserCreateRequest.class))).thenReturn(userDTO);
        when(userModelAssembler.toModel(any(UserDTO.class))).thenReturn(entityModel);


        //then
        assertNotNull(userController.addUser(userCreateRequest));
    }

    @Test
    void canDeleteUser() {
        //when
        doNothing().when(userService).deleteUser(any(Integer.class));
        userController.deleteUser(any(Integer.class));
        //then
        verify(userService).deleteUser(any(Integer.class));
    }

    @Test
    void canUpdateUser() {
        //given
        UserUpdateRequest userUpdateRequest = new UserUpdateRequest(userDTO.name(), userDTO.email());
        //when
        when(userService.updateUser(any(Integer.class), any(UserUpdateRequest.class))).thenReturn(userDTO);
        when(userModelAssembler.toModel(any(UserDTO.class))).thenReturn(entityModel);

        //then
        assertNotNull(userController.updateUser(userUpdateRequest, 1));
    }
}