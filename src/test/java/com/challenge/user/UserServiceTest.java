package com.challenge.user;

import com.challenge.user.exception.UserNotFoundException;
import com.challenge.user.model.User;
import com.challenge.user.model.UserDTOMapper;
import com.challenge.user.request.UserCreateRequest;
import com.challenge.user.request.UserUpdateRequest;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.*;

import java.time.LocalDateTime;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

class UserServiceTest {
    @Mock
    private UserRepository userRepository;

    @Spy
    private UserDTOMapper userDTOMapper;

    @InjectMocks
    private UserService userService;

    private AutoCloseable autoCloseable;
    private User user;

    @BeforeEach
    void setUp() {
        autoCloseable = MockitoAnnotations.openMocks(this);
        userService = new UserService(userRepository, userDTOMapper);
        //Init user
        user = new User();
        user.setUserId(1);
        user.setUserName("George");
        user.setUserEmail("george@test.com");
        user.setCreatedDate(LocalDateTime.now());
        user.setLastUpdatedDate(LocalDateTime.now());
    }

    @AfterEach
    void tearDown() throws Exception {
        autoCloseable.close();
    }

    @Test
    void canGetAllUsers() {
        //when
//        when(userRepository.findAll()).thenReturn(List.of(user));
        userService.getAllUsers();
        //then
        verify(userRepository).findAll();
    }

    @Test
    void canGetUserById() {
        //when
        when(userRepository.findById(any(Integer.class))).thenReturn(Optional.of(user));
        userService.getUserById(any(Integer.class));
        //then
        verify(userRepository).findById(any(Integer.class));
    }

    @Test
    void getUserByIdWillThrowWhenUserWasNotFound() {
        when(userRepository.findById(any(Integer.class))).thenReturn(Optional.empty());
        assertThrows(UserNotFoundException.class, () -> userService.getUserById(any(Integer.class)));
    }

    @Test
    void canCreateUser() {
        //given
        UserCreateRequest userCreateRequest = new UserCreateRequest(user.getUserName(), user.getUserEmail());
        //when
        when(userRepository.save(any(User.class))).thenReturn(user);
        userService.createUser(userCreateRequest);
        //then
        ArgumentCaptor<User> userArgumentCaptor = ArgumentCaptor.forClass(User.class);
        verify(userRepository).save(userArgumentCaptor.capture());
        User capturedUser = userArgumentCaptor.getValue();
        assertNotNull(capturedUser);
    }

    @Test
    void canUpdateUserWhenIsFound() {
        //given
        UserUpdateRequest userUpdateRequest = new UserUpdateRequest(user.getUserName(), user.getUserEmail());
        //when
        when(userRepository.save(any(User.class))).thenReturn(user);
        when(userRepository.findById(any(Integer.class))).thenReturn(Optional.ofNullable(user));
        userService.updateUser(user.getUserId(), userUpdateRequest);
        //then
        ArgumentCaptor<User> userArgumentCaptor = ArgumentCaptor.forClass(User.class);
        verify(userRepository).save(userArgumentCaptor.capture());
        verify(userRepository).findById(any(Integer.class));
        User capturedUser = userArgumentCaptor.getValue();
        assertNotNull(capturedUser);
    }

    @Test
    void canUpdateUserWhenIsNotFound() {
        //given
        UserUpdateRequest userUpdateRequest = new UserUpdateRequest(user.getUserName(), user.getUserEmail());
        //when
        when(userRepository.save(any(User.class))).thenReturn(user);
        userService.updateUser(user.getUserId(), userUpdateRequest);
        //then
        ArgumentCaptor<User> userArgumentCaptor = ArgumentCaptor.forClass(User.class);
        verify(userRepository).save(userArgumentCaptor.capture());
        User capturedUser = userArgumentCaptor.getValue();
        assertNotNull(capturedUser);
    }

    @Test
    void canDeleteUser() {
        //when
        doNothing().when(userRepository).deleteById(any(Integer.class));
        userService.deleteUser(any(Integer.class));
        //then
        verify(userRepository).deleteById(any(Integer.class));

    }
}