package com.challenge.user;

import com.challenge.user.model.UserDTO;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.hateoas.EntityModel;

import java.time.LocalDateTime;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.instanceOf;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

class UserModelAssemblerTest {
    private UserDTO userDTO;
    private EntityModel<UserDTO> expectedEntityModel;
    private final UserModelAssembler underTest = new UserModelAssembler();

    @BeforeEach
    void setUp() {
        //Init userDTO
        userDTO = new UserDTO(1, "James",
                "james@test.com", LocalDateTime.now());
        //Init entityModel
        expectedEntityModel = EntityModel.of(userDTO,
                linkTo(methodOn(UserController.class).getUserById(userDTO.id())).withSelfRel(),
                linkTo(methodOn(UserController.class).getUsers()).withRel("users"));
    }

    @Test
    void toModel() {
        //given
        UserDTO newUserDTO = new UserDTO(1, "James",
                "james@test.com", LocalDateTime.now());
        //when
        EntityModel<UserDTO> result = underTest.toModel(newUserDTO);

        //then
        assertThat(result, instanceOf(EntityModel.class));
        assertNotNull(result);
    }
}