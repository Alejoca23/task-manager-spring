package com.challenge.user;


import com.challenge.user.model.UserDTO;
import com.challenge.user.request.UserCreateRequest;
import com.challenge.user.request.UserUpdateRequest;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.IanaLinkRelations;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;


@RestController
@RequestMapping("api/v1/users")
public class UserController {
    private final UserService userService;
    private final UserModelAssembler userModelAssembler;

    public UserController(UserService userService, UserModelAssembler userModelAssembler) {
        this.userService = userService;
        this.userModelAssembler = userModelAssembler;
    }


    @GetMapping
    public CollectionModel<EntityModel<UserDTO>> getUsers() {
        List<EntityModel<UserDTO>> users = userService.getAllUsers()
                .stream().map(userModelAssembler::toModel)
                .toList();
        return CollectionModel.of(users,
                linkTo(methodOn(UserController.class).getUsers()).withSelfRel());
    }

    @GetMapping("{userId}")
    public EntityModel<UserDTO> getUserById(@PathVariable("userId") Integer id) {
        return userModelAssembler.toModel(userService.getUserById(id));
    }

    @PostMapping
    public ResponseEntity<?> addUser(@RequestBody UserCreateRequest request) {
        EntityModel<UserDTO> userModel = userModelAssembler.toModel(userService.createUser(request));
        return ResponseEntity.created(userModel.getRequiredLink(IanaLinkRelations.SELF).toUri())
                .body(userModel);
    }

    @DeleteMapping("{userId}")
    public ResponseEntity<?> deleteUser(@PathVariable("userId") Integer id) {
        userService.deleteUser(id);
        return ResponseEntity.noContent().build();
    }

    @PutMapping("{userId}")
    public ResponseEntity<?> updateUser(@RequestBody UserUpdateRequest request, @PathVariable("userId") Integer id) {
        EntityModel<UserDTO> userModel = userModelAssembler.toModel(userService.updateUser(id, request));
        return ResponseEntity.created(userModel.getRequiredLink(IanaLinkRelations.SELF).toUri())
                .body(userModel);
    }
}