package com.challenge.user;

import com.challenge.user.exception.UserNotFoundException;
import com.challenge.user.model.User;
import com.challenge.user.model.UserDTO;
import com.challenge.user.model.UserDTOMapper;
import com.challenge.user.request.UserCreateRequest;
import com.challenge.user.request.UserUpdateRequest;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.Comparator;
import java.util.List;

@Service
public class UserService {
    private final UserRepository userRepository;
    private final UserDTOMapper userDTOMapper;


    public UserService(UserRepository userRepository, UserDTOMapper userDTOMapper) {
        this.userRepository = userRepository;
        this.userDTOMapper = userDTOMapper;
    }

    public List<UserDTO> getAllUsers() {
        return userRepository.findAll()
                .stream()
                .map(userDTOMapper)
                .sorted(Comparator.comparingInt(UserDTO::id))
                .toList();
    }

    public UserDTO getUserById(Integer id) {
        return userRepository.findById(id)
                .stream().map(userDTOMapper).findFirst()
                .orElseThrow(() -> new UserNotFoundException(id));
    }

    public UserDTO createUser(UserCreateRequest userCreateRequest) {
        User user = new User(
                userCreateRequest.name().toUpperCase(),
                userCreateRequest.email().toLowerCase(),
                LocalDateTime.now(),
                LocalDateTime.now());
        user = userRepository.save(user);

        return new UserDTO(user.getUserId(), user.getUserName(),
                user.getUserEmail(), user.getLastUpdatedDate());
    }

    public UserDTO updateUser(Integer id, UserUpdateRequest userUpdateRequest) {
        return userRepository.findById(id)
                .map(updateUser -> {
                    updateUser.setUserName(userUpdateRequest.name().toUpperCase());
                    updateUser.setUserEmail(userUpdateRequest.email().toLowerCase());
                    updateUser.setLastUpdatedDate(LocalDateTime.now());
                    updateUser = userRepository.save(updateUser);
                    return new UserDTO(updateUser.getUserId(), updateUser.getUserName(),
                            updateUser.getUserEmail(), updateUser.getLastUpdatedDate());
                }).orElseGet(() -> {
                    User newUser = new User(userUpdateRequest.name(),
                            userUpdateRequest.email(),
                            LocalDateTime.now(),
                            LocalDateTime.now());
                    newUser = userRepository.save(newUser);
                    return new UserDTO(newUser.getUserId(), newUser.getUserName(),
                            newUser.getUserEmail(), newUser.getLastUpdatedDate());
                });
    }

    public void deleteUser(Integer id) {
        userRepository.deleteById(id);
    }
}