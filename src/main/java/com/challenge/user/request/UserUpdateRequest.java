package com.challenge.user.request;

public record UserUpdateRequest(String name, String email) {
}
