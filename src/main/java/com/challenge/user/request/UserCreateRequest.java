package com.challenge.user.request;

public record UserCreateRequest(String name, String email) {
}
