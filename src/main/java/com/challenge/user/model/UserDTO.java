package com.challenge.user.model;

import java.time.LocalDateTime;

public record UserDTO(Integer id, String name,
                      String email, LocalDateTime lastUpdate) {
}