package com.challenge.user.model;


import jakarta.persistence.*;
import lombok.Data;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import java.time.LocalDateTime;

@Data
@Entity
@Table(name = "users")
public class User {
    public User(String name, String email, LocalDateTime createdDate, LocalDateTime lastUpdatedDate) {
        this.userName = name;
        this.userEmail = email;
        this.createdDate = createdDate;
        this.lastUpdatedDate = lastUpdatedDate;
    }

    public User() {

    }

    @Id
    @SequenceGenerator(name = "id_user_seq", sequenceName = "id_user_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "id_user_seq")
    @Column(name = "user_id")
    private Integer userId;
    @Column(name = "user_name", nullable = false, length = 30)
    private String userName;
    @Column(name = "user_email", nullable = false, length = 30)
    private String userEmail;
    @CreationTimestamp
    @Column(name = "user_create_date")
    private LocalDateTime createdDate;

    @UpdateTimestamp
    @Column(name = "user_updated_date")
    private LocalDateTime lastUpdatedDate;

//    @OneToMany(orphanRemoval = true)
//    private ArrayList<Task> tasks;

}
