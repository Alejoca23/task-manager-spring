package com.challenge.task.exception;

public class TaskNotFoundException extends RuntimeException {
    public TaskNotFoundException(Integer id) {
        super("Could not find task with ID " + id);
    }
}
