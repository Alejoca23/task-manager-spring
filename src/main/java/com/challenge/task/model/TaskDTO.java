package com.challenge.task.model;

import com.challenge.user.model.UserDTO;

import java.time.LocalDateTime;

public record TaskDTO(Integer id, String title,
                      String description, LocalDateTime lastUpdate,
                      TaskStatus status, UserDTO userDTO) {
}
