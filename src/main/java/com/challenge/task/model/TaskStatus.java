package com.challenge.task.model;

public enum TaskStatus {
    TODO("To do"), IN_PROGRESS("In progress"), DONE("Done");

    private String status;

    TaskStatus(String status) {
        this.status = status;
    }

    public String getStatus() {
        return status;
    }
}
