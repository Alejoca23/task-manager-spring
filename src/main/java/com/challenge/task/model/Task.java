package com.challenge.task.model;


import com.challenge.user.model.User;
import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import lombok.Data;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import org.hibernate.annotations.UpdateTimestamp;

import java.time.LocalDateTime;

@Data
@Entity
@Table(name = "tasks")
public class Task {
    @Id
    @SequenceGenerator(name = "id_task_seq", sequenceName = "id_task_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "id_task_seq")
    @Column(name = "task_id")
    private Integer taskId;
    @Column(name = "task_title", nullable = false, length = 30)
    private String taskTitle;
    @Column(name = "task_description", nullable = false, length = 30)
    private String taskDescription;
    @CreationTimestamp
    @Column(name = "task_create_date")
    private LocalDateTime createdDate;
    @UpdateTimestamp
    @Column(name = "task_updated_date")
    private LocalDateTime lastUpdatedDate;
    @Enumerated(EnumType.STRING)
    @Column(name = "task_status")
    private TaskStatus status;


    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "user_id", nullable = false)
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JsonIgnore
    private User user;

    public Task() {
    }

    public Task(String title, String description, LocalDateTime createdDate, LocalDateTime lastUpdatedDate, TaskStatus status, User user) {
        this.taskTitle = title;
        this.taskDescription = description;
        this.createdDate = createdDate;
        this.lastUpdatedDate = lastUpdatedDate;
        this.status = status;
        this.user = user;
    }
};
