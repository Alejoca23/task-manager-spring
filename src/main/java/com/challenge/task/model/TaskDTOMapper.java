package com.challenge.task.model;

import com.challenge.user.model.UserDTOMapper;
import org.springframework.stereotype.Service;

import java.util.function.Function;

@Service
public class TaskDTOMapper implements Function<Task, TaskDTO> {
    @Override
    public TaskDTO apply(Task task) {
        return new TaskDTO(task.getTaskId(), task.getTaskTitle(),
                task.getTaskDescription(), task.getLastUpdatedDate(),
                task.getStatus(), new UserDTOMapper().apply(task.getUser()));
    }
}
