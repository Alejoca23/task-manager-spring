package com.challenge.task;


import com.challenge.task.exception.TaskNotFoundException;
import com.challenge.task.model.Task;
import com.challenge.task.model.TaskDTO;
import com.challenge.task.model.TaskDTOMapper;
import com.challenge.task.model.TaskStatus;
import com.challenge.task.request.TaskCreateRequest;
import com.challenge.task.request.TaskUpdateRequest;
import com.challenge.user.UserRepository;
import com.challenge.user.exception.UserNotFoundException;
import com.challenge.user.model.User;
import com.challenge.user.model.UserDTOMapper;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;

@Service
public class TaskService {
    private final UserRepository userRepository;
    private final TaskRepository taskRepository;
    private final TaskDTOMapper taskDTOMapper;

    public TaskService(UserRepository userRepository, TaskRepository taskRepository, TaskDTOMapper taskDTOMapper) {
        this.userRepository = userRepository;
        this.taskRepository = taskRepository;
        this.taskDTOMapper = taskDTOMapper;
    }

    public List<TaskDTO> getAllTasks(Integer userId) {
        return taskRepository.findAll()
                .stream().filter(
                        task -> Objects.equals(task.getUser().getUserId(), userId)
                ).map(taskDTOMapper)
                .sorted(Comparator.comparingInt(TaskDTO::id))
                .toList();
    }

    public TaskDTO getTask(Integer taskId) {
        return taskRepository.findById(taskId)
                .map(taskDTOMapper)
                .orElseThrow(
                        () -> new TaskNotFoundException(taskId));
    }

    public TaskDTO createTask(TaskCreateRequest request) {
        User user = userRepository.findById(request.userId())
                .orElseThrow(() -> new UserNotFoundException(request.userId()));
        Task task = taskRepository.save(new Task(request.title(), request.description(), LocalDateTime.now(),
                LocalDateTime.now(), TaskStatus.TODO, user));
        return new TaskDTO(task.getTaskId(), task.getTaskTitle(), task.getTaskDescription(),
                task.getLastUpdatedDate(), task.getStatus(), new UserDTOMapper().apply(task.getUser()));
    }

    public TaskDTO updateTask(Integer taskId, TaskUpdateRequest request) {
        User user = userRepository.findById(request.userId())
                .orElseThrow(() -> new UserNotFoundException(request.userId()));

        return taskRepository.findById(taskId)
                .map(updateTask -> {
                    updateTask.setTaskTitle(request.title());
                    updateTask.setTaskDescription(request.description());
                    updateTask.setLastUpdatedDate(LocalDateTime.now());
                    updateTask = taskRepository.save(updateTask);
                    return new TaskDTO(updateTask.getTaskId(), updateTask.getTaskTitle(),
                            updateTask.getTaskDescription(),
                            updateTask.getLastUpdatedDate(), updateTask.getStatus(),
                            new UserDTOMapper().apply(updateTask.getUser()));
                }).orElseGet(() -> {
                            Task newTask = taskRepository.save(new Task(request.title(), request.description(), LocalDateTime.now(),
                                    LocalDateTime.now(), TaskStatus.TODO, user));
                            return new TaskDTO(newTask.getTaskId(), newTask.getTaskTitle(),
                                    newTask.getTaskDescription(),
                                    newTask.getLastUpdatedDate(), newTask.getStatus(),
                                    new UserDTOMapper().apply(newTask.getUser()));
                        }
                );
    }

    public void deleteTask(Integer id) {
        taskRepository.deleteById(id);
    }

    public TaskDTO taskInProgress(Integer taskId) {
        Task task = taskRepository.findById(taskId)
                .orElseThrow(() -> new TaskNotFoundException(taskId));
        task.setStatus(TaskStatus.IN_PROGRESS);
        task.setLastUpdatedDate(LocalDateTime.now());
        task = taskRepository.save(task);
        return new TaskDTO(task.getTaskId(), task.getTaskTitle(), task.getTaskDescription(),
                task.getLastUpdatedDate(), task.getStatus(), new UserDTOMapper().apply(task.getUser()));

    }

    public TaskDTO taskDone(Integer taskId) {
        Task task = taskRepository.findById(taskId)
                .orElseThrow(() -> new TaskNotFoundException(taskId));
        task.setStatus(TaskStatus.DONE);
        task.setLastUpdatedDate(LocalDateTime.now());
        task = taskRepository.save(task);
        return new TaskDTO(task.getTaskId(), task.getTaskTitle(), task.getTaskDescription(),
                task.getLastUpdatedDate(), task.getStatus(), new UserDTOMapper().apply(task.getUser()));
    }
}