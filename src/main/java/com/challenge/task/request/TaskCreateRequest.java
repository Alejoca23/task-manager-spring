package com.challenge.task.request;

public record TaskCreateRequest(Integer userId, String title, String description) {

}
