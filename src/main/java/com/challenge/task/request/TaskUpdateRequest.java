package com.challenge.task.request;

public record TaskUpdateRequest(Integer userId, String title, String description) {
}