package com.challenge.task;


import com.challenge.task.model.TaskDTO;
import com.challenge.task.model.TaskStatus;
import com.challenge.task.request.TaskCreateRequest;
import com.challenge.task.request.TaskUpdateRequest;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.IanaLinkRelations;
import org.springframework.hateoas.MediaTypes;
import org.springframework.hateoas.mediatype.problem.Problem;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@RestController
@RequestMapping("api/v1/tasks")
public class TaskController {
    private final TaskService taskService;
    private final TaskModelAssembler taskModelAssembler;

    public TaskController(TaskService taskService, TaskModelAssembler taskModelAssembler) {
        this.taskService = taskService;
        this.taskModelAssembler = taskModelAssembler;
    }


    @GetMapping
    public CollectionModel<EntityModel<TaskDTO>> getTasks(@RequestParam("userId") Integer userId) {
        //TODO Implement an exception (user doesn't have any task)
        List<EntityModel<TaskDTO>> tasks = taskService.getAllTasks(userId)
                .stream().map(taskModelAssembler::toModel)
                .toList();
        return CollectionModel.of(tasks,
                linkTo(methodOn(TaskController.class).getTasks(userId)).withSelfRel());
    }

    @GetMapping("{taskId}")
    public EntityModel<TaskDTO> getTaskById(@PathVariable("taskId") Integer taskId) {
        return taskModelAssembler.toModel(taskService.getTask(taskId));
    }

    @PostMapping
    public ResponseEntity<?> createTask(@RequestBody TaskCreateRequest request) {
        EntityModel<TaskDTO> taskModel = taskModelAssembler.toModel(taskService.createTask(request));
        return ResponseEntity.created(taskModel.getRequiredLink(IanaLinkRelations.SELF).toUri())
                .body(taskModel);
    }

    @PutMapping("{taskId}")
    public ResponseEntity<?> updateTask(@PathVariable("taskId") Integer taskId, @RequestBody TaskUpdateRequest request) {
        //TODO Implement an exception (user and task don't exist)
        EntityModel<TaskDTO> taskModel = taskModelAssembler.toModel(taskService.updateTask(taskId, request));
        return ResponseEntity.created(taskModel.getRequiredLink(IanaLinkRelations.SELF).toUri())
                .body(taskModel);
    }

    @DeleteMapping("{taskId}")
    public ResponseEntity<?> deleteTask(@PathVariable("taskId") Integer taskId) {
        taskService.deleteTask(taskId);
        return ResponseEntity.noContent().build();
    }

    @PutMapping("{taskId}/in-progress")
    public ResponseEntity<?> taskInProgressStatus(@PathVariable("taskId") Integer taskId) {
        TaskDTO task = taskService.getTask(taskId);

        if (task.status() == TaskStatus.TODO)
            return ResponseEntity.ok(taskModelAssembler.toModel(taskService.taskInProgress(taskId)));

        return ResponseEntity.status(HttpStatus.METHOD_NOT_ALLOWED)
                .header(HttpHeaders.CONTENT_TYPE, MediaTypes.HTTP_PROBLEM_DETAILS_JSON_VALUE)
                .body(Problem.create()
                        .withTitle("Method not allowed")
                        .withDetail("You can't in progress a task with " + task.status() + " status"));
    }

    @PutMapping("{taskId}/done")
    public ResponseEntity<?> taskDoneStatus(@PathVariable("taskId") Integer taskId) {
        TaskDTO task = taskService.getTask(taskId);

        if (task.status() == TaskStatus.TODO || task.status() == TaskStatus.IN_PROGRESS)
            return ResponseEntity.ok(taskModelAssembler.toModel(taskService.taskDone(taskId)));

        return ResponseEntity.status(HttpStatus.METHOD_NOT_ALLOWED)
                .header(HttpHeaders.CONTENT_TYPE, MediaTypes.HTTP_PROBLEM_DETAILS_JSON_VALUE)
                .body(Problem.create()
                        .withTitle("Method not allowed")
                        .withDetail("You can't done a task with " + task.status() + " status"));
    }
}