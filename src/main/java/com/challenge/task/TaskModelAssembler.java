package com.challenge.task;

import com.challenge.task.model.TaskDTO;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.server.RepresentationModelAssembler;
import org.springframework.stereotype.Component;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@Component
public class TaskModelAssembler implements RepresentationModelAssembler<TaskDTO, EntityModel<TaskDTO>> {
    @Override
    public EntityModel<TaskDTO> toModel(TaskDTO taskDTO) {
        return EntityModel.of(taskDTO,
                linkTo(methodOn(TaskController.class).getTaskById(taskDTO.id())).withSelfRel(),
                linkTo(methodOn(TaskController.class).getTasks(taskDTO.userDTO().id())).withRel("tasks"));
    }
}